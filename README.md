# PTSpriteEx

> A spritesheet extractor for Pony Town

After reverse engineering efforts rewarded me with some intuition on howw the game identifies and loads sprites in its spritesheets, PTSpriteEx was built as a way of confirming these theories as well as locating new sprites added to the game between updates.

## Installation

### Prerequisites

- Python 3 (tested on Python 3.7)
- `virtualenv` (`pip install virtualenv`)

### Preliminary setup

```bash
$ cd path/to/project
$ virtualenv .env
$ ./.env/bin/activate # or ./.env/Scripts/activate on Windows
$ pip install -r requirements.txt
```

## Usage

### Obtaining a spritesheet

To use PTSpriteEx, you need to obtain a copy of a Pony Town spritesheet you want to extract, as well as the array used to store the spritesheet information. To download the spritesheet:

From `pony.town`:  
1. Sign in to your account
2. Join the game
3. Open the developer tools
4. Go to the Sources tab (or equivalent)
5. Under `assets/images`, find `[spritesheet]-[rev].png` and save it

From `beta.pony.town`:  
1. Open the developer tools
2. Go to the Sources tab (or equivalent)
3. Under `assets/scripts`, open `bootstrap-[rev].js`
4. Use the "prettify" button (and prepare for lag!)
5. Once the script is prettified, CTRL + F and search for the spritesheet name (and prepare for more lag!)
6. Find the object which has key-value pairs in the format: `"[spritesheet]": "[rev]"`
7. Copy the `[rev]` part of this object, and construct the following link: `https://beta.pony.town/assets/images/[spritesheet]-[rev].png`
8. Right-click and save

### Obtaining the metadata

1. Open the developer tools
2. Go to the Sources tab (or equivalent)
3. Under `assets/scripts`, open `bootstrap-[rev].js`
4. Use the "prettify" button (and prepare for lag!)
5. Once the script is prettified, CTRL + F and search for the spritesheet name (and prepare for more lag!)
6. Find the object with format:
   ```js
   {
       src: "images/[spritesheet].png",
       data: void 0,
       tex: void 0,
       sprites: xX, // or some sort of weird two-letter variable name
       palette: !0 // or !1; remember this value
   }
   ```
7. Find the declaration of the two-letter variable in the `sprites` key (you may want to save your formatted source file and open it in VS Code instead to make this easier)
8. Copy the ENTIRE hex string (or long array) inside the weird function call
9. Create a new file named `[spritesheet]-[rev].png.json` in the same location as your `[spritesheet]-[rev].png` with the following format:
   ```json
   {
      "type": "rgb",       // or "other" if the palette key from before was !1
      "newEncoding": true, // if your spritesheet is older than the 2019-01-22 beta.pony.town update, !!!set this to false!!!
	   "sprites": ""        // that really long string/array goes here
   }
   ```

### Extracting the spritesheet

```bash
$ ./main.py path/to/[spritesheet]-[rev].png
```

A directory will be created named `[spritesheet]-[rev]` containing all of the extracted sprites. If the spritesheet type was `rgb`, additional directories (`red`, `green`, and `blue`) will be created for sprites that were defined as being one of those types. Any sprites without a specific color association will be written to the root of the directory. File names indicate the MD5 hash of the sprite they contain.

## License

Copyright (c) 2018-19, Eliot Partridge. Licensed under [the MIT License](/LICENSE).

Pony Town is (c) Agamnentzar.