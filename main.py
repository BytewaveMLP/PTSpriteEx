import argparse
import hashlib
import json
import os
import sys
from enum import IntEnum
from PIL import Image
from functools import partial
from collections import namedtuple

parser = argparse.ArgumentParser(description='Extract sprites from Pony Town spritesheets')
parser.add_argument('spritesheet', help='the spritesheet (pony/pony2) to extract - must have accompanying descriptor file (filename.json) which must contain type field (rgb or color) and sprites (massive flat array of numbers)', type=str)
args = parser.parse_args()

if not os.path.isfile(args.spritesheet):
	print('Spritesheet file missing', file=sys.stderr)
	exit(1)

image = Image.open(args.spritesheet)

if not os.path.isfile(args.spritesheet + '.json'):
	print('Descriptor file missing for spritesheet ' + args.spritesheet, file=sys.stderr)
	exit(1)

with open(args.spritesheet + '.json', 'r') as descriptor:
	try:
		spritesheet_data = json.load(descriptor)
	except:
		print('Failed to parse spritesheet descriptor; invalid JSON?', file=sys.stderr)
		exit(2)

Sprite = namedtuple('Sprite', ['x', 'y', 'w', 'h', 'ox', 'oy', 'type'])

# if spritesheetData.get('type') == 'rgb' and 'palette' not in spritesheetData:
# 	print('Spritesheet descriptor claims spritesheet is of type rgb but does not provide palette data', file=sys.stderr)
# 	exit(2)

class Ho():
	"""
	Helper class which emulates JS closure scoping rules needed for part of the extraction process.
	"""

	def __init__(self, e):
		self.t = 0
		self.n = 0
		self.e = e
	
	def process(self, r):
		"""
		A reimplementation of the inner function returned by the original function in JS
		"""

		if r & ~0b11111:
			raise ValueError("Invalid bit count")
		o = 0
		while r != 0:
			if self.n == 0:
				self.n = 8
				self.t = self.e()
			i = self.n if self.n < r else r
			o = o << i | self.t >> self.n - i & 255 >> 8 - i
			r -= i
			self.n -= i
		return o

def process_sprite_data_new(data_str):
	n = 0
	t = set()
	def extract_byte(data_str):
		nonlocal n
		byte = int(data_str[n:n+2], 16)
		n += 2
		return byte
	r = Ho(partial(extract_byte, data_str))
	while n < len(data_str):
		t.add(Sprite(
			x=r.process(12),
			y=r.process(12),
			w=r.process(9),
			h=r.process(9),
			ox=r.process(8),
			oy=r.process(8),
			type=r.process(6)
		))
	return t

def process_sprite_data_old(data_arr):
	processed = set()
	for n in range(0, len(data_arr), 7):
		processed.add(Sprite(
			x=data_arr[n],
			y=data_arr[n + 1],
			w=data_arr[n + 2],
			h=data_arr[n + 3],
			ox=data_arr[n + 4],
			oy=data_arr[n + 5],
			type=data_arr[n + 6]
		))
	return processed

class SpriteType(IntEnum):
	UNKNOWN = -1
	NONE    = 0
	RED     = 3
	BLUE    = 1
	GREEN   = 4

	@classmethod
	def has_value(cls, value):
		return any(value == item.value for item in cls)

sprite_data_raw = spritesheet_data.get('sprites')

if spritesheet_data.get('newEncoding', False) == True:
	sprite_info = process_sprite_data_new(sprite_data_raw)
else:
	sprite_info = process_sprite_data_old(sprite_data_raw)

dirname = '.'.join(args.spritesheet.split('.')[:-1])
try:
	os.mkdir(dirname)
	if spritesheet_data.get('type') == 'rgb':
		os.mkdir(dirname + '/red')
		os.mkdir(dirname + '/green')
		os.mkdir(dirname + '/blue')
except:
	print('Failed to create output directory ' + dirname, file=sys.stderr)
	exit(3)

for sprite in sprite_info:
	sprite_type = sprite.type
	if not SpriteType.has_value(sprite_type):
		print("ERROR: Unknown sprite type:", sprite_type, file=sys.stderr)
		sprite_type = SpriteType.UNKNOWN.value

	crop_rect = (sprite.x, sprite.y, sprite.x + sprite.w, sprite.y + sprite.h)
	sprite_img = image.crop(crop_rect)

	out_optional = ''

	if spritesheet_data.get('type') == 'rgb' and sprite_type not in [SpriteType.NONE.value, SpriteType.UNKNOWN.value]:
		sprite_img = sprite_img.convert('RGBA')
		data = sprite_img.load()
		for y in range(sprite_img.size[1]):
			for x in range(sprite_img.size[0]):
				red, green, blue, alpha = data[x, y]
				desired = green
				if sprite_type == SpriteType.RED.value:
					desired = red
					out_optional = 'red'
				elif sprite_type == SpriteType.GREEN.value:
					desired = green
					out_optional = 'green'
				elif sprite_type == SpriteType.BLUE.value:
					desired = blue
					out_optional = 'blue'
				data[x, y] = (desired, desired, desired, alpha)

	outfile = dirname + '/' + (out_optional + '/' if out_optional != '' else '') + hashlib.md5(sprite_img.tobytes()).hexdigest() + '.png'
	print('Writing ' + outfile + '...')
	sprite_img.save(outfile)
